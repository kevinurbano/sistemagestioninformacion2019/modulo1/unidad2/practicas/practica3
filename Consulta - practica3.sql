﻿USE practica3;

/* 
  1.- Visualizar el número de empleados de cada departamento. 
      Utilizar GROUP BY para agrupar por departamento. 
*/
  SELECT 
    e.dept_no, COUNT(*) nEmpleados 
    FROM 
      emple e 
    GROUP BY 
      e.dept_no;



/* 
  2.- Visualizar los departamentos con más de 5 empleados. 
      Utilizar GROUP BY para agrupar por departamento y HAVING para establecer la condición sobre los grupos.
*/
  SELECT 
    e.dept_no, COUNT(*) nEmpleados 
    FROM 
      emple e 
    GROUP BY 
      e.dept_no
    HAVING
      nEmpleados>5;

/*
  3.- Hallar la media de los salarios de cada departamento (utilizar la función avg y GROUP BY).  
*/
  SELECT 
    e.dept_no,AVG(e.salario) salarioMedio
    FROM 
      emple e
    GROUP BY 
      e.dept_no;

/*
  4.- Visualizar el nombre de los empleados vendedores del departamento ʻVENTASʼ 
      (Nombre del departamento=ʼVENTASʼ, oficio=ʼVENDEDORʼ).  
*/
  SELECT 
    DISTINCT e.apellido 
    FROM 
      emple e 
    JOIN 
      depart d ON e.dept_no = d.dept_no
    WHERE
      d.dnombre="VENTAS" AND e.oficio="VENDEDOR"; 

/*
  5.- Visualizar el número de vendedores del departamento ʻVENTASʼ 
      (utilizar la función COUNT sobre la consulta anterior).  
*/
  SELECT 
    COUNT(*) nEmpleados
    FROM 
      (
        SELECT 
          DISTINCT e.apellido 
          FROM 
            emple e 
          JOIN 
            depart d ON e.dept_no = d.dept_no
          WHERE
            d.dnombre="VENTAS" AND e.oficio="VENDEDOR"
      ) c1;

/*
  6.- Visualizar los oficios de los empleados del departamento ʻVENTASʼ  
*/
  SELECT 
    DISTINCT e.oficio
    FROM 
      emple e
    JOIN
      depart d ON e.dept_no = d.dept_no
    WHERE d.dnombre="VENTAS";

/*
  7.- A partir de la tabla EMPLE, visualizar el número de empleados de cada departamento cuyo oficio sea ʻEMPLEADOʼ
      (utilizar GROUP BY para agrupar por departamento. En la cláusula WHERE habrá que indicar que el oficio es ʻEMPLEADOʼ).  
*/
  SELECT 
    e.dept_no,COUNT(*) nEmpleados
    FROM 
      emple e 
    WHERE 
      e.oficio="EMPLEADO" 
    GROUP BY
      e.dept_no;

/*
  8.- Visualizar el departamento con más empleados.    
*/
  -- C1
  SELECT 
    e.dept_no,COUNT(*) nEmpleados 
    FROM 
      emple e
    WHERE 
      e.oficio="EMPLEADO"
    GROUP BY 
      e.dept_no;
  
  -- C2
  SELECT 
    MAX(c1.nEmpleados)
    FROM
      (
        SELECT 
          e.dept_no,COUNT(*) nEmpleados 
          FROM 
            emple e
          WHERE 
            e.oficio="EMPLEADO"
          GROUP BY 
            e.dept_no    
      ) c1;
  
  -- Final
  SELECT 
    e.dept_no
    FROM 
      emple e
    WHERE 
      e.oficio="EMPLEADO"
    GROUP BY 
      e.dept_no
    HAVING
      COUNT(*)=
        (         
          SELECT 
            MAX(c1.nEmpleados)
            FROM
              (
                SELECT 
                  e.dept_no,COUNT(*) nEmpleados 
                  FROM 
                    emple e
                  WHERE 
                    e.oficio="EMPLEADO"
                  GROUP BY 
                    e.dept_no    
              ) c1
        );

/*
  9.- Mostrar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados.
*/
  -- C1
  SELECT 
    e.dept_no,SUM(e.salario) sumSalario
    FROM
      emple e 
    GROUP BY 
      e.dept_no;
  
  -- C2
  SELECT 
    AVG(e.salario) mediaSalario
    FROM 
      emple e;

  -- Final
  SELECT 
    e.dept_no
    FROM
      emple e 
    GROUP BY 
      e.dept_no
    HAVING 
      SUM(e.salario) > 
        (
          SELECT 
            AVG(e.salario) mediaSalario
            FROM 
              emple e
        );
   
/* 10.- Para cada oficio obtener la suma de salarios. */
  SELECT 
    e.oficio, SUM(e.salario) sumaSalario
    FROM 
      emple e 
    GROUP BY
      e.oficio;

/* 11.- Visualizar la suma de salarios de cada oficio del departamento ʻVENTASʼ */
  SELECT 
    e.oficio, SUM(e.salario) sumaSalario
    FROM 
      emple e 
    JOIN 
      depart d 
    ON 
      e.dept_no = d.dept_no 
    WHERE 
      d.dnombre="VENTAS" 
    GROUP BY 
      e.oficio;

/* 12.- Visualizar el número de departamento que tenga más empleados cuyo oficio sea empleado. */
  -- C1
  SELECT 
    e.dept_no,COUNT(*) numEmple 
    FROM 
      emple e 
    WHERE 
      e.oficio="EMPLEADO" 
    GROUP BY 
      e.dept_no;

  -- C2
  SELECT 
    MAX(c1.numEmple) maximo
    FROM 
      (
        SELECT 
          e.dept_no,COUNT(*) numEmple 
          FROM 
            emple e 
          WHERE 
            e.oficio="EMPLEADO" 
          GROUP BY 
            e.dept_no
      ) c1;

  -- Final
  SELECT 
    e.dept_no
    FROM 
      emple e 
    WHERE 
      e.oficio="EMPLEADO" 
    GROUP BY 
      e.dept_no
    HAVING
      COUNT(*)=
        (
          SELECT 
            MAX(c1.numEmple) maximo
            FROM 
              (
                SELECT 
                  e.dept_no,COUNT(*) numEmple 
                  FROM 
                    emple e 
                  WHERE 
                    e.oficio="EMPLEADO" 
                  GROUP BY 
                    e.dept_no
              ) c1
        );
/* 13.- Mostrar el número de oficios distintos de cada departamento.*/
  SELECT 
    e.dept_no,COUNT(DISTINCT e.oficio) 
    FROM 
      emple e 
    GROUP BY 
      e.dept_no;

/* 14.- Mostrar los departamentos que tengan más de dos personas trabajando en la misma profesión. */
  SELECT 
    e.dept_no,e.oficio,COUNT(*) numEmple 
    FROM 
      emple e
    GROUP BY
      e.dept_no,e.oficio
    HAVING numEmple>2; 

/* 15.- Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades. */
  SELECT 
    h.estanteria, SUM(unidades) sumUnidades
    FROM 
      herramientas h 
    GROUP BY 
      h.estanteria;

/* 16.- Visualizar la estantería con más unidades de la tabla HERRAMIENTAS. (con totales y sin totales). */
  -- C1
  SELECT 
    h.estanteria, SUM(unidades) sumUnidades
    FROM 
      herramientas h 
    GROUP BY 
      h.estanteria;

  -- C2
  SELECT 
    MAX(c1.sumUnidades) 
    FROM
      (
        SELECT 
          h.estanteria, SUM(unidades) sumUnidades
          FROM 
            herramientas h 
          GROUP BY 
            h.estanteria
      ) c1;
  
  -- Final
  SELECT 
    h.estanteria
    FROM 
      herramientas h 
    GROUP BY 
      h.estanteria
    HAVING
      SUM(h.unidades)=
        (
          SELECT 
            MAX(c1.sumUnidades) 
            FROM
              (
                SELECT 
                  h.estanteria, SUM(unidades) sumUnidades
                  FROM 
                    herramientas h 
                  GROUP BY 
                    h.estanteria
              ) c1
        );

/* 17.- Mostrar el número de médicos que pertenecen a cada hospital, ordenado por número descendente de hospital.*/
  SELECT 
    m.cod_hospital,COUNT(*) numMedicos
    FROM 
      medicos m 
    GROUP BY 
      m.cod_hospital
    ORDER BY 
      m.cod_hospital DESC;

/* 18.- Realizar una consulta en la que se muestre por cada hospital el nombre de las especialidades que tiene. */
  SELECT 
    DISTINCT m.cod_hospital, m.especialidad 
    FROM 
      medicos m;

/* 19.- Realizar una consulta en la que aparezca por cada hospital y en cada especialidad
        el número de médicos (tendrás que partir de la consulta anterior y utilizar GROUP BY). */
  SELECT 
    m.cod_hospital, m.especialidad 
    FROM 
      medicos m
    GROUP BY m.cod_hospital,m.especialidad;

/* 20.- Obtener por cada hospital el número de empleados. */
  SELECT 
    m.cod_hospital,COUNT(*) numEmple 
    FROM 
      medicos m 
    GROUP BY 
      m.cod_hospital;

/* 21.- Obtener por cada especialidad el número de trabajadores. */
  SELECT 
    m.especialidad,COUNT(*) numEmple 
    FROM 
      medicos m 
    GROUP BY 
      m.especialidad;

/* 22.- Visualizar la especialidad que tenga más médicos. */
  -- C1
  SELECT 
    m.especialidad,COUNT(*) numEmple 
    FROM 
      medicos m 
    GROUP BY 
      m.especialidad;

  -- C2
  SELECT 
    MAX(c1.numEmple)
    FROM
      (
        SELECT 
          m.especialidad,COUNT(*) numEmple 
          FROM 
            medicos m 
          GROUP BY 
            m.especialidad 
      ) c1;
  
  -- Final
  SELECT 
    m.especialidad
    FROM 
      medicos m 
    GROUP BY 
      m.especialidad
    HAVING
      COUNT(*)=
        (
          SELECT 
            MAX(c1.numEmple)
            FROM
              (
                SELECT 
                  m.especialidad,COUNT(*) numEmple 
                  FROM 
                    medicos m 
                  GROUP BY 
                    m.especialidad 
              ) c1
        );

/* 23.- ¿Cuál es el nombre del hospital que tiene mayor número de plazas? */
  -- C1
  SELECT 
    MAX(h.num_plazas) 
    FROM 
      hospitales h;

  -- Final
  SELECT 
    h.nombre
    FROM 
      hospitales h
    WHERE
      h.num_plazas=
        (
          SELECT 
            MAX(h.num_plazas) 
            FROM 
              hospitales h
        );


/* 24.- Visualizar las diferentes estanterías de la tabla HERRAMIENTAS 
        ordenados descendentemente por estantería. */
  SELECT 
    DISTINCT h.estanteria 
    FROM 
      herramientas h 
    ORDER BY 
      h.estanteria DESC;

/* 25.- Averiguar cuántas unidades tiene cada estantería. */
  SELECT 
    h.estanteria, SUM(h.unidades) numUnidades
    FROM 
      herramientas h
    GROUP BY
      h.estanteria;

/* 26.- Visualizar las estanterías que tengan más de 15 unidades */
  SELECT 
    h.estanteria, SUM(h.unidades) numUnidades
    FROM 
      herramientas h
    GROUP BY
      h.estanteria
    HAVING
      numUnidades>15;

/* 27.- ¿Cuál es la estantería que tiene más unidades? */
  -- C1
  SELECT 
    h.estanteria, SUM(h.unidades) numUnidades
    FROM 
      herramientas h
    GROUP BY
      h.estanteria;

  -- C2
  SELECT 
    MAX(c1.numUnidades) 
    FROM
      (
        SELECT 
          h.estanteria, SUM(h.unidades) numUnidades
          FROM 
            herramientas h
          GROUP BY
            h.estanteria
      ) c1;

  -- Final
  SELECT 
    h.estanteria
    FROM 
      herramientas h
    GROUP BY
      h.estanteria
    HAVING
      SUM(h.unidades)=
        (
          SELECT 
            MAX(c1.numUnidades) 
            FROM
              (
                SELECT 
                  h.estanteria, SUM(h.unidades) numUnidades
                  FROM 
                    herramientas h
                  GROUP BY
                    h.estanteria
              ) c1
        );

/* 28.- A partir de las tablas EMPLE y DEPART mostrar los datos del departamento que no tiene ningún empleado. */
  -- C1
  SELECT 
    DISTINCT d.dept_no 
    FROM 
      emple e 
    JOIN 
      depart d ON e.dept_no = d.dept_no;
  
  -- Final
  SELECT 
    d.dept_no
    FROM
      depart d
    LEFT JOIN
      (
        SELECT 
          DISTINCT d.dept_no 
          FROM 
            emple e 
          JOIN 
            depart d ON e.dept_no = d.dept_no
      ) C1 ON C1.dept_no=d.dept_no
    WHERE c1.dept_no IS NULL;

/* 29.- Mostrar el número de empleados de cada departamento. 
        En la salida se debe mostrar también los departamentos que no tienen ningún empleado. */
  SELECT 
    d.dept_no,COUNT(e.emp_no) numEmple
    FROM 
      depart d
    LEFT JOIN
      emple e ON d.dept_no = e.dept_no
    GROUP BY d.dept_no;

/* 30.- Obtener la suma de salarios de cada departamento,
        mostrando las columnas DEPT_NO, SUMA DE SALARIOS y DNOMBRE. 
        En el resultado también se deben mostrar los departamentos que no tienen asignados empleados. */
  SELECT 
    d.dept_no,SUM(e.salario),d.dnombre 
    FROM
      depart d
    LEFT JOIN
      emple e ON d.dept_no = e.dept_no
    GROUP BY
      d.dept_no;

/* 31.- Utilizar la función IFNULL en la consulta anterior para que en el caso de que un departamento no tenga empleados, 
        aparezca como suma de salarios el valor 0.*/
  SELECT 
    d.dept_no,IFNULL(SUM(e.salario),0) sumSalario
    FROM 
      depart d
    LEFT JOIN
      emple e ON d.dept_no = e.dept_no
    GROUP BY d.dept_no;
     
/* 32.- Obtener el número de médicos que pertenecen a cada hospital, 
        mostrando las columnas COD_HOSPITAL, NOMBRE y NÚMERO DE MÉDICOS.
        En el resultado deben aparecer también los datos de los hospitales que no tienen médicos. */
  SELECT 
    h.cod_hospital,h.nombre,COUNT(m.dni) numEmple
    FROM
      hospitales h
    LEFT JOIN
      medicos m ON h.cod_hospital = m.cod_hospital
    GROUP BY
      h.cod_hospital;

     

